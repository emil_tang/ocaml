open QCheck

let rec sum' = function
  | [] -> 0
  | x :: xs -> x + sum' (xs)

let rec merge xl yl =
  match (xl, yl) with
  | x, [] -> x
  | [], y -> y
  | x :: xs, y :: ys when x >= y -> y :: merge xl ys
  | x :: xs, y :: ys -> x :: merge xs yl

let rec gcd' n m = match (n, m) with
  | n, 0 -> n
  | n, m when n > m -> gcd' (n - m) m
  | n, m -> gcd' n (m - n)

let rec gcd_or a b =
  let r = a mod b in
  if r = 0
  then b
  else gcd_or b r

let rec fib_slow = function
  | 0 -> 0
  | 1 -> 1
  | n -> fib_slow (n - 1) + fib_slow (n - 2)

let fib_faster n =
  let rec aux k p1 p2 =
    if k = n
    then p1
    else aux (k + 1) p2 (p1 + p2)
  in aux 0 0 1

let fib_constant n =
  let g = (1. +. sqrt 5.) /. 2. and b = Float.of_int (n) in
  let a = (g ** b -. (1. -. g) ** b) /. sqrt 5. in
  int_of_float (snd (modf a))

let empty = []

let rec del k = function
  | [] -> []
  | x :: xs when fst x = k -> del k xs
  | x :: xs -> x :: del k xs

let rec find k = function
  | [] -> None
  | x :: xs when fst x = k -> Some (snd x)
  | x :: xs -> find k xs

let rec add k v = function
  | [] -> [(k , v)]
  | x :: xs when fst x = k -> add k v xs
  | x :: xs -> x :: add k v xs

let sum_test = Test.make ~name:"Sum Test"
    (pair (list int) (list int))
    (fun (xs, ys) -> sum'(xs @ ys) = (sum' xs) + (sum' ys))

let merge_test = Test.make ~name:"Merge Test"
    (pair (list int) (list int))
    (fun (xs, ys) ->
       List.sort compare (xs @ ys) =
       merge (List.sort compare  xs) (List.sort compare ys))

let int64_test = Test.make ~name:"Int64 Test"
    int (fun i -> i = Int64.to_int (Int64.of_int i))

let int32_test = Test.make ~name:"Int32 Test"
    int (fun i -> i = Int32.to_int (Int32.of_int i))

let int_to_string_test = Test.make ~name:"Int To String Test"
    int (fun i -> i = int_of_string (string_of_int i))

let gcd_test = Test.make ~name:"GCD Test"
    (pair small_int small_int) (fun (n,m) -> gcd_or n m = gcd' n m)

let fib_dynamic_test = Test.make small_int (fun n -> fib_slow (n) = fib_faster (n))

let fib_constant_test= Test.make small_int (fun n -> fib_slow (n) = fib_constant (n))
;;

del "x" [("x",0);("y",1)];;
find "x" [("x",0);("y",1)];;
add "z" 3 [("x",0);("y",1)];;
add "x" 4 [("x",0);("y",1)];;

let dict1 = add "x" 1 empty;;
let dict2 = add "y" 2 dict1;;
let dict3 = add "x" 3 dict2;;
let dict4 = add "y" 4 dict3;;

let _ = QCheck_runner.run_tests ~verbose:true [
    sum_test;
    merge_test;
    int32_test;
    int64_test;
    int_to_string_test;
    gcd_test;
    fib_constant_test;
    fib_dynamic_test;
  ];;
