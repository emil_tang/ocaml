open QCheck

let myshr i = 
  if i <> 0 
  then Iter.append (Iter.return (i/2)) (Iter.return (i-1))
  else Iter.empty;;

let test_false = Test.make ~name:"Test False" 
    (set_shrink myshr int) (fun i -> false);;
let test_lower = Test.make ~name:"Test Lower" 
 (set_shrink myshr int) (fun i -> i < 432);;

QCheck_runner.run_tests [test_lower;];;

