open Format

let cube x = x * x * x;;
let is_even x = x mod 2 == 0;;
let quadroot x = sqrt (sqrt x);;

let  my_sum xs = 
  let rec aux acc xs = 
    match xs with 
    | [] -> acc
    | x :: xs -> aux (acc + x) xs  
  in aux 0 xs ;;

(*let l = List.init 20000000 (fun f -> f);;*)

let msb =
  let rec aux acc = function
    | 0 -> acc
    | x -> aux (acc + 1) (x lsr 1) 
  in aux 0;;

let rec msb' = function
  | 0 -> 0
  | x -> 1 + msb' (x lsr 1);;

let fst' (x, _) = x;;
let snd' (_, y) = y;;

msb 5;;
msb 2;;
msb 0;;

msb' 5;;
msb' 2;;
msb' 0;;

let t = (1, 2);;

fst' t;;
snd' t;;

let rec sum' = function 
  | [] -> 0
  | x :: xs -> x + sum' (xs);; 

let rec member' y = function
  | [] -> false
  | x :: _ when  y = x -> true
  | _ :: xs -> member' y xs;;

let l = [1;2;3];;

sum' l;;
member' 1 l;;
