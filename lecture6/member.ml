open QCheck

let list_gen = make (fun _ -> 
    Gen.(generate1 (list_size (int_bound 1_000_000) small_int))
  )
;;

let rec member y = function
  | [] -> false
  | x :: xs -> x = y || member y xs
;;

let rec member' y = function
  | [] -> false
  | x :: _ when x = y -> true
  | _ :: xs -> member' y xs

let rec fac = function
  | 0 -> 1
  | n -> n * fac (n - 1)
;;

let rec reverse = function
  | [] -> []
  | x :: xs -> (reverse xs) @ [x]
;;

let fac' n =
  let rec loop acc = function
    | 0 -> acc
    | i -> loop (n * acc) (i - 1)
  in loop 1 n
;;

let rev' xs =
  let rec loop acc = function
    | [] -> acc
    | x :: xs -> loop (x :: acc) xs
  in loop [] xs
;;

let test_fac_non_tail = Test.make ~name:"Factorial Non Tail Rec"
    (small_nat)
    (fun x -> fac x = x || true)
;;

let test_rev_non_tail = Test.make ~name:"Reverse non tail"
    (list_gen)
    (fun xs -> reverse xs = List.rev xs)
;;

let test_member = Test.make ~name:"Member Test"
    (list_gen)
    (fun xs -> member 1 xs)
;;

let test_member' = Test.make ~name:"Member' Test"
    (list_gen)
    (fun xs -> member' 1 xs)
;;

let test_fac_tail = Test.make ~name:"Fac tail"
    (small_nat)
    (fun x -> fac' x = x || true)
;;

let test_rev_tail = Test.make ~name:"rev tail"
    (list_gen)
    (fun xs -> rev' xs = List.rev xs)
;;

QCheck_runner.run_tests ~verbose:true [
  test_member;
  test_member';
  test_fac_non_tail;
  test_rev_non_tail;
  test_rev_tail;
  test_fac_tail;
];;

