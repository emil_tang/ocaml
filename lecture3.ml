open QCheck

let my_int_of_string s =
  try Some (int_of_string s)
  with Failure _ -> None
;;

my_int_of_string "ABC";;

let my_list_find p l =
  let k = List.find_opt p l in
  match k with
  | None -> raise Not_found
  | Some l -> l
;;

my_list_find (fun e -> e>4) [1;5;4;];;

type color = Red | Black;;
type 'a rbtree = Leaf | Node of color * 'a * 'a rbtree * 'a rbtree;;

let empty = Leaf;;

(*  mem : 'a -> 'a set -> bool  *)
let rec member x s = match s with
  | Leaf -> false
  | Node (_, y, left, right) ->
    x = y || (x < y && member x left) || (x > y && member x right)
;;

(*  balance : color * 'a * ('a tree) * ('a tree) -> 'a tree  *)
let balance t = match t with
  | Black, z, Node (Red, y, Node (Red, x, a, b), c), d
  | Black, z, Node (Red, x, a, Node (Red, y, b, c)), d
  | Black, x, a, Node (Red, z, Node (Red, y, b, c), d)
  | Black, x, a, Node (Red, y, b, Node (Red, z, c, d)) ->
    Node (Red, y, Node (Black, x, a, b), Node (Black, z, c, d))
  | color, x, a, b -> Node (color, x, a, b)
;;

(*  insert : 'a -> 'a set -> 'a set  *)
let insert x s =
  let rec ins s = match s with
    | Leaf -> Node (Red, x, Leaf, Leaf)
    | Node (color, y, a, b) ->
      if x < y then balance (color, y, ins a, b)
      else if x > y then balance (color, y, a, ins b)
      else s (* x = y *) in
  match ins s with (* guaranteed to be non-empty *)
  | Node (_,y,a,b) -> Node (Black, y, a, b)
  | Leaf -> raise (Invalid_argument "insert: cannot color empty tree")
;;

(*  set_of_list : 'a list -> 'a set  *)
let rec set_of_list = function
  | [] -> empty
  | x :: l -> insert x (set_of_list l)
;;

let rec tree_to_string s = match s with
  | Leaf -> ""
  | Node (_, y, left, right) ->
    tree_to_string left ^ string_of_int y ^ "; " ^ tree_to_string right
;;

let set = set_of_list [1;4;5;6;8;2;1;];;

tree_to_string set;;

let rec no_red_parent = function
  | Leaf -> true
  | Node(Red, _, Node(Red, _, _, _), _)
  | Node(Red, _, _, Node(Red, _, _, _)) -> false
  | Node(_, _, left, right) -> no_red_parent left && no_red_parent right
;;

no_red_parent set;;

no_red_parent
  (Node(Red, 1, Node(Red, 2, Leaf, Leaf), Node(Black, 3, Leaf, Leaf)));;

let same_length_paths s =
  let rec count n = function
    | Leaf -> n :: []
    | Node(Black, _, left, right) -> count (n + 1) left @ count(n + 1) right
    | Node(Red, _, left, right) -> count n left @ count n right in
  let rec contains k = function
    | [] -> false
    | x :: xs when x = k -> true
    | x :: xs  -> contains k xs in
  let rec contains_all bucket = function
    | [] -> true
    | (x :: xs) as list ->
      contains x (bucket @ list) && contains_all (x :: bucket) xs in
  contains_all [] (count 0 s)
;;

same_length_paths set;;

type aexp =
  | X
  | Lit of int
  | Plus of aexp * aexp
  | Times of aexp * aexp
  | Minus of aexp * aexp
;;

let rec interpret xval ae = match ae with
  | X -> xval
  | Lit i -> i
  | Plus (ae0, ae1) ->
    let v0 = interpret xval ae0 in
    let v1 = interpret xval ae1 in
    v0 + v1
  | Times (ae0, ae1) ->
    let v0 = interpret xval ae0 in
    let v1 = interpret xval ae1 in
    v0 * v1
  | Minus (ae0, ae1) ->
    let v0 = interpret xval ae0 in
    let v1 = interpret xval ae1 in
    v0 - v1
;;

let rec size' = function
  | X
  | Lit _ -> 1
  | Plus (a, b)
  | Times (a, b)
  | Minus (a, b) -> 1 + size' a + size' b
;;

let mytree = Plus (Lit 1, Times (X, Lit 3))
;;

size' mytree;;

let leafgen = Gen.oneof [Gen.return X; Gen.map (fun i -> Lit i) Gen.int];;

let mygen' = Gen.sized (Gen.fix(fun recgen n -> match n with
    | 0 -> leafgen
    | n -> Gen.frequency [
        (1, leafgen);
        (2, Gen.map2 (fun l r -> Plus (l, r))
           (recgen (n / 2)) (recgen (n / 2)));
        (2, Gen.map2 (fun l r -> Times (l, r))
           (recgen (n / 2)) (recgen (n / 2)));
      ]
  ))
;;

let size_test =
  let size_gen = make ~stats:[("size", size')] mygen' in
  Test.make ~name:"Size Test" size_gen (fun _ -> true)
;;

type inst =
  | Load
  | Push of int
  | Add
  | Mult
  | Sub
;;

(* our compiler from arithmetic expressions to instructions *)
let rec compile ae = match ae with
  | X -> [Load]
  | Lit i -> [Push i]
  | Plus (ae0, ae1) ->
    let is0 = compile ae0 in
    let is1 = compile ae1 in
    is0 @ is1 @ [Add]
  | Times (ae0, ae1) ->
    let is0 = compile ae0 in
    let is1 = compile ae1 in
    is0 @ is1 @ [Mult]
  | Minus  (ae0, ae1) ->
    let is0 = compile ae0 in
    let is1 = compile ae1 in
    is0 @ is1 @ [Sub]
;;

exception Empty_Stack;;
exception Not_Enough_Items_On_Stack;;

let rec run reg inst stack =
  match (inst, stack) with
  | [], stack -> stack
  | Load :: inst, stack -> run reg inst (reg :: stack)
  | Push i :: inst, stack -> run reg inst (i :: stack)
  | Add :: inst, a :: b :: stack -> run reg inst (a + b :: stack)
  | Mult :: inst, a :: b :: stack -> run reg inst (a * b :: stack)
  | Sub :: inst, a :: b :: stack ->
    run reg inst (a - b :: stack)
  | (( Add | Mult | Sub ) :: _), _ :: [] ->
    raise Not_Enough_Items_On_Stack
  | _, [] -> raise Empty_Stack
;;

run 4 [Push 3; Load; Add;] [5; 1;];;

let compile_run_test = Test.make ~name:"Compile Run Test"
    (make mygen') (fun l -> run 0 (compile l) [] = [interpret 0 l])
;;

let check_sub = Test.make ~name:"Subtract Test"
    (pair int int) (fun (a, b) -> run 0 [Sub] [a; b;] = [a - b])
;;

let my_int_gen = Gen.oneof [
    Gen.int;
    Gen.small_signed_int;
    Gen.oneofl Gen.int_corners;
  ]
;;

let test_my_int_gen =
  let gen = make ~stats:[("it", fun t -> t)] my_int_gen in
  Test.make gen (fun _ -> true)
;;

let test_no_red_parent = Test.make ~name:"Test No Red Parent"
    (list int) (fun l -> no_red_parent (set_of_list l))
;;

let test_same_lenght_paths = Test.make ~name:"Test Same Length Paths"
    (list int) (fun l -> same_length_paths (set_of_list l))
;;

QCheck_runner.run_tests ~verbose:true [
  test_no_red_parent;
  test_same_lenght_paths;
  size_test;
  check_sub;
  compile_run_test;
  test_my_int_gen;
];;

